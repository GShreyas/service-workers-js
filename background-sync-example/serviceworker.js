self.oninstall = function(event) {
    console.error('oninstall');
    caches.open('backgroundSyncSample2')
    .then(function(cache) {
        cache.addAll([
            '/',
            './../index.html',
            './bundle.js'
        ])
        .catch(function(err) {
            console.log('files not added ', err);
        })
    })
    .catch(function(err) {
        console.log('err ', err);
    })
}

self.onfetch = function(event) {
    console.error('onfetch');
    event.respondWith(
        console.error('event.request',event.request);
        caches.match(event.request)
        .then(function(cachedFiles) {
            if(cachedFiles) {
                return cachedFiles;
            } else {
                return fetch(event.request);
            }
        })
        .catch(function(err) {
            console.log('err in fetch ', err);
        })
    );
}

self.onsync = function(event) { //fired when the page (or worker) that registered the event with the SyncManager is running and as soon as network connectivity is available.
    console.error('onsync');
    if(event.tag == 'example-sync') {
        event.waitUntil(syncIt()); //We're also using event.waitUntil. Because a service worker isn't continually running -- it "wakes up" to do a task and then "goes back to sleep" -- we want to use event.waitUntil to keep the service worker active. 
    }
}

// down here are the other functions to go get the indexeddb data and also post to our server

function syncIt() {
    return getIndexedDB()
    .then(sendToServer)
    .catch(function(err) {
        return err;
    })
}

function getIndexedDB() {
    return new Promise(function(resolve, reject) {
        var db = indexedDB.open('newsletterSignup');
        db.onsuccess = function(event) {
            this.result.transaction("newsletterObjStore").objectStore("newsletterObjStore").getAll().onsuccess = function(event) {
                resolve(event.target.result);
            }
        }
        db.onerror = function(err) {
            reject(err);
        }
    });
}

function sendToServer(response) {
    return fetch('https://www.mocky.io/v2/5c0452da3300005100d01d1f', {
            method: 'POST',
            body: JSON.stringify(response),
            headers:{
              'Content-Type': 'application/json'
            }
    }).then(function(rez2) {
        return rez2.text();
    }).catch(function(err) {
        return err;
    })
}